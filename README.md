# AntTest
AntTest is a unit testing framework for [Apache Ant](http://ant.apache.org/) build systems. It provides Ant tasks like `<test:assert />` which enable you to unit test your build system. We unit test our production code so why not unit test the code that production depends on to be built? Unit testing code helps us break the build system down into more testable (and inherently more readable and maintainable), you will quickly find it hard to test a monolithic build system with targets in the range of hundreds of lines long.

This testing framework works best with build systems which break their functionality up into smaller more manageable macros and have small targets which are used for dependency chaining and directly calling into macros.

## Usage
To build the testing framework you will need to clone this repository and build the Java based tasks, this can be done through the terminal with the commands below;

```bash
git clone https://bitbucket.org/IAmTehCreator/ant-test.git
cd ant-test
ant publish
```

You will notice that the `publish` target will run all of the test framework unit tests which are stored in `test.xml` (they should all pass).

Once the library has been packaged you will find a file `anttest.jar` in the root directory, this file can be used by Apache Ant build scripts by including the following in their build script;

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="antlib:org.apache.tools.ant" xmlns:test="com.smilne.test">

	<taskdef resource="antlib.xml" classpath="anttest.jar" uri="com.smilne.test" />

</project>
```

## Testing
To write unit tests for your build system you are best creating a new build file or a set of build files in a test directory if you've got a lot of XML to test. Include the test framework in each test file like above.

Below is an example build file that declares and uses a macro `<create-metadata />`. We can write unit tests for this macro to ensure it's doing it's job correctly by creating another build file called `test.xml` and loading it in the `run.tests` target.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="antlib:org.apache.tools.ant" xmlns:sf="com.salesforce" name="project">

	<macrodef name="create-metadata" description="Creates a metadata file for each class">
		<attribute name="dir" description="The class directory" />
		<sequential>
			<for param="file">
				<fileset dir="@{dir}" includes="*.cls" />
				<sequential>
					<sf:template-class-metadata file="@{file}-meta.xml" version="41.0" />
				</sequential>
			</for>
		</sequential>
	</macrodef>

	<target name="build">
		<create-metadata dir="src/classes" />
	</target>

	<target name="run.tests">
		<ant antfile="test.xml" target="run.all" />
	</target>
</project>
```

Below is the `test.xml` file that contains unit tests for this macro. There are two unit tests, one to ensure the `<sf:template-class-metadata />` macro is being called for each class and another to ensure the correct version is being passed. We are able to write these tests easier by mocking out the implementation of the dependant macro. When a macro is mocked all calls to it will be recorded and stubbed allowing us to inspect how many times it was called and what attribute values it was called with.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="antlib:org.apache.tools.ant" xmlns:sf="com.salesforce" xmlns:test="com.smilne.test" name="test">
	<taskdef resource="antlib.xml" classpath="anttest.jar" uri="com.smilne.test" />

	<target name="run.all" description="Runs all tests" />

	<test:describe name="create-metadata">
		<!-- This is run before any of the tests in this describe -->
		<test:before-all>
			<!-- Create some test files to work with -->
			<mkdir dir="test/src/classes"/>
			<echo file="test/src/classes/MyClass.cls">public class MyClass {}</echo>
			<echo file="test/src/classes/MyOtherClass.cls">public class MyOtherClass {}</echo>

			<!-- Mock out another macro and record calls to it -->
			<test:mock macro="template-class-metadata" uri="com.salesforce">
				<test:attribute name="file" />
				<test:attribute name="version" />
			</test:mock>
		</test:before-all>

		<!-- This is run before each test executes -->
		<test:before-each>
			<!-- Resets the mock recorder for each test -->
			<test:mock-reset mock="template-class-metadata" />
		</test:before-each>

		<test:it should="create metadata file for each class">
			<create-metadata dir="test/src/classes" />

			<test:assert got="${mocks.template-class-metadata.calls}" expected="2" message="Should call metadata template twice" />
			<test:assert got="${mocks.template-class-metadata.call.1.file}"
				expected="test/src/classes/MyClass.cls-meta.xml"
				message="Should pass in first class name" />

			<test:assert got="${mocks.template-class-metadata.call.2.file}"
				expected="test/src/classes/MyOtherClass.cls-meta.xml"
				message="Should pass in second class name" />
		</test:it>

		<test:it should="use metadata version 41.0">
			<create-metadata dir="test/src/classes" />

			<test:assert got="${mocks.template-class-metadata.call.1.version}" expected="41.0" />
		</test:it>

		<!-- This is run after the last test has finished -->
		<test:after-all>
			<delete dir="test/src/classes" />
		</test:after-all>
	</test:describe>

</project>
```

## License
This framework uses the MIT license which you can view in `LICENSE`, it is permissive and allows you to use this code however you see fit so long as you preserve the copyright notices and license.
