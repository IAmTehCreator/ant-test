/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne;

public class Logger
{
	public static final Integer COLOUR_BLACK = 30;
	public static final Integer COLOUR_RED = 31;
	public static final Integer COLOUR_GREEN = 32;
	public static final Integer COLOUR_YELLOW = 33;
	public static final Integer COLOUR_BLUE = 34;
	public static final Integer COLOUR_MAGENTA = 35;
	public static final Integer COLOUR_CYAN = 36;
	public static final Integer COLOUR_WHITE = 37;

	public static final Integer ATTRIBUTE_RESET = 0;
	public static final Integer ATTRIBUTE_BRIGHT = 1;
	public static final Integer ATTRIBUTE_DIM = 2;
	public static final Integer ATTRIBUTE_UNDERLINE = 3;
	public static final Integer ATTRIBUTE_LINK = 5;
	public static final Integer ATTRIBUTE_REVERSE = 7;
	public static final Integer ATTRIBUTE_HIDDEN = 8;

	private static final Integer INDENTATION_AMOUNT = 4;

	private static Integer indentation = 0;

	/**
	 * Logs the given message to the terminal using the default settings
	 * @param message The message to log to the terminal
	 */
	public static void log(String message)
	{
		log(message, COLOUR_WHITE, ATTRIBUTE_BRIGHT);
	}

	/**
	 * Logs the given message to the terminal in the specified ANSI colour.
	 * @param message The message to log to the terminal
	 * @param colour The foreground colour to print the text in
	 */
	public static void log(String message, Integer colour)
	{
		log(message, colour, ATTRIBUTE_BRIGHT);
	}

	/**
	 * Logs the given message to the terminal in the specified ANSI colour and
	 * with the specified ANSI attribute.
	 * @param message The message to log to the terminal
	 * @param colour The foreground colour to print the text in
	 * @param attribute The ANSI attribute value
	 */
	public static void log(String message, Integer colour, Integer attribute)
	{
		System.out.println(getIndentation() + "\033[" + attribute + ";" + colour + "m" + message + "\033[0m");
	}

	/**
	 * Increases the amount of indentation applied to messages to be logged.
	 */
	public static void indent()
	{
		indentation += INDENTATION_AMOUNT;
	}

	/**
	 * Decreases the amount of indentation applied to messages to be logged.
	 */
	public static void dedent()
	{
		indentation -= INDENTATION_AMOUNT;

		if(indentation < 0)
		{
			indentation = 0;
		}
	}

	/**
	 * Returns a string padded with the current indentation value of spaces.
	 * @return A string of spaces
	 */
	private static String getIndentation()
	{
		String spaces = "";

		for(Integer i = 0; i < indentation; i++)
		{
			spaces = spaces + " ";
		}

		return spaces;
	}
}
