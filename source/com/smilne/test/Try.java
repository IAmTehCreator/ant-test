/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import java.util.*;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.TaskContainer;

/**
 * This class implements an Ant task which catches any exceptions thrown from nested tasks
 * and allows them to be handled. When an exception is encountered the nested <test:catch>
 * task is invoked and the exception message is loaded into a property ${exception.message}
 * by default.
 *
 * Supported Attributes;
 *  - var            Optional, the name of the variable to load with the exception message
 *  - ignoreAsserts  Optional, when true exceptions thrown from failed asserts will not be caught
 */
public class Try extends Task implements TaskContainer
{
	private static final String TASK_CATCH = "test:catch";
	private static final String DEFAULT_VAR = "exception.message";
	private static final String ASSERT_SELECTOR = "FAILED:";

	private List children;
	private Task catchTask;
	private String varName;
	private Boolean ignoreAsserts;

	/**
	 * Default constructor
	 */
	public Try()
	{
		super();

		children = new ArrayList();
		varName = DEFAULT_VAR;
		ignoreAsserts = false;
	}

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		try
		{
			executeChildren();
		}
		catch(Exception e)
		{
			if(shouldCatch(e))
			{
				getProject().setProperty(varName, e.getMessage());
				catchTask.perform();
			}
		}
	}

	/**
	 * TaskContainer addTask() implementation. This is called by Ant for each child
	 * element nested under this one.
	 * @param child The child element added to this container
	 */
	public void addTask(Task child)
	{
		if(child.getTaskName() == TASK_CATCH)
		{
			catchTask = child;
			return;
		}

		children.add(child);
	}

	// Attribute setters

	public void setVar(String varName)
	{
		this.varName = varName;
	}

	public void setIgnoreAsserts(String ignoreAsserts)
	{
		this.ignoreAsserts = ignoreAsserts.equals("true");
	}

	// Helper Methods

	/**
	 * Iterates over all child elements and invokes them
	 */
	private void executeChildren()
	{
		for ( Integer i = 0; i < children.size(); i++ )
		{
			((Task)children.get(i)).perform();
		}
	}

	/**
	 * Determines if the given exception should be caught given the current attribute
	 * values.
	 * @return True if the exception should be caught, false if it should be re-thrown
	 */
	private Boolean shouldCatch(Exception e)
	{
		// Never catch if no catch clause has been given
		if(catchTask == null)
		{
			return false;
		}

		// Always catch if not from an assert statement
		if(!e.getMessage().contains(ASSERT_SELECTOR))
		{
			return true;
		}

		return !ignoreAsserts;
	}
}
