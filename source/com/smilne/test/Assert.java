/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import org.apache.tools.ant.Task;
import org.apache.tools.ant.BuildException;

import com.smilne.Logger;

/**
 * This class implements an Ant task which will perform an assertion and fail the build if
 * the assertion is not met. Different comparisons will be used depending on which
 * attributes have been specified. If a message is not specified then one will be generated
 * from the values specified in the attributes.
 *
 * Supported attributes are;
 *  - got       The value to assert
 *  - expected  Optional, the value to assert equality against
 *  - contains  Optional, the value expected to be contained within the got value
 *  - message   Optional, the message to print when the assertion is performed
 *  - suppress  Private, for internal use when unit testing the framework, DO NOT USE
 */
public class Assert extends Task
{
	private static final String PASSED = "PASSED: ";
	private static final String FAILED = "FAILED: ";

	private String got;
	private String expected;
	private String contains;
	private String message;
	private Boolean isSuppressed;

	/**
	 * Default constructor
	 */
	public Assert()
	{
		super();

		isSuppressed = false;
	}

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		defaultValues();

		if(doAssert())
		{
			Logger.log(PASSED + message, Logger.COLOUR_GREEN);
			return;
		}

		String exceptionMessage = FAILED + message;
		if(!isSuppressed)
		{
			Logger.log(exceptionMessage, Logger.COLOUR_RED);
		}

		throw new BuildException(exceptionMessage);
	}

	// Attribute Setters

	public void setGot(String value)
	{
		this.got = value;
	}

	public void setExpected(String value)
	{
		this.expected = value;
	}

	public void setContains(String value)
	{
		this.contains = value;
	}

	public void setMessage(String value)
	{
		this.message = value;
	}

	public void setSuppress(String value)
	{
		this.isSuppressed = value.equals("true");
	}

	// Helper Methods

	/**
	 * Sets the default values for the attributes.
	 */
	private void defaultValues()
	{
		if(got == null)
			got = "true";

		if(expected == null && contains == null)
			expected = "true";

		if(message == null)
			message = getMessage();
	}

	/**
	 * Performs an assertion determining the comparison method from the values
	 * stored in the attributes.
	 * @return The result of the comparison
	 */
	private Boolean doAssert()
	{
		if(expected != null)
		{
			return got.equals(expected);
		}

		if(contains != null)
		{
			return got.contains(contains);
		}

		return false;
	}

	/**
	 * Gets the message specified in the attributes, or if one was not specified
	 * then it is generated.
	 * @return The generated or specified assertion message
	 */
	private String getMessage()
	{
		if(message != null)
		{
			return message;
		}

		if(expected != null)
		{
			return "Expected '" + expected + "' but got '" + got + "'";
		}

		if(contains != null)
		{
			return "Expected '" + got + "' to contain '" + contains + "'";
		}

		return "Unknown assertion";
	}
}
