/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.MacroDef;

/**
 * This class implements an Ant task that will take values for a macro mock.
 *
 * NOTE: NOT TO BE USED OUTSIDE <test:mock>
 */
public class Attribute extends Task
{
	public static final String ATTRIBUTE_NAME = "name";
	public static final String ATTRIBUTE_DEFAULT = "default";

	private String name;
	private String defaultValue;

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		// Do nothing, this task isn't supposed to be executed as-is, it should
		// first be converted to a MacroDef.Attribute.
	}

	// Attribute Setters

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setDefault(String defaultValue)
	{
		this.defaultValue = defaultValue;
	}

	public String getDefault()
	{
		return defaultValue;
	}
}
