/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import java.util.*;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.TaskContainer;

/**
 * This class implements a basic Ant task which can contain child element. This task will only
 * invoke it's child elements, it will not perform any logic.
 */
public class BasicContainer extends Task implements TaskContainer
{
	private List children;

	/**
	 * Default constructor
	 */
	public BasicContainer()
	{
		super();

		children = new ArrayList();
	}

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		executeChildren();
	}

	/**
	 * TaskContainer addTask() implementation. This is called by Ant for each child
	 * element nested under this one.
	 * @param child The child element added to this container
	 */
	public void addTask(Task child)
	{
		children.add(child);
	}

	// Helper Methods

	/**
	 * Iterates over all child elements and invokes them.
	 */
	private void executeChildren()
	{
		for ( Integer i = 0; i < children.size(); i++ )
		{
			((Task)children.get(i)).perform();
		}
	}
}
