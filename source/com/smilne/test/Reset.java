/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import org.apache.tools.ant.Task;

/**
 * This class implements an Ant task that will reset an integer counter to zero, the name of
 * the variable that contains the counter must be specified.
 *
 * Supported Attributes;
 *  - var  The name of the property that contains the counter to reset
 */
public class Reset extends Task
{
	public String var;

	/**
	 * Default constructor
	 */
	public Reset()
	{
		super();
	}

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		getProject().setProperty(var, Integer.toString(0));
	}

	// Attribute Setters

	public void setVar(String var)
	{
		this.var = var;
	}
}
