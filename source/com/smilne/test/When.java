/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import java.util.*;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.TaskContainer;

/**
 * This class implements an Ant task which allows basic logic to be performed. The task takes
 * tasks nested within a <test:do> and <test:otherwise> tag and will invoke the correct branch
 * depending on the logic it is to perform. You can check for equality or for a substring.
 *
 * Supported attributes are;
 *  - value     The value to check
 *  - is        Optional, if specified the value is checked for equality with this
 *  - contains  Optional, if specified the value is checked to see if it contains this
 */
public class When extends Task implements TaskContainer
{
	public String value;
	public String expected;
	public String contains;

	private List doTasks;
	private List otherwiseTasks;

	/**
	 * Default constructor
	 */
	public When()
	{
		super();

		doTasks = new ArrayList();
		otherwiseTasks = new ArrayList();
	}

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		if ( doComparison() )
		{
			for ( Integer i = 0; i < doTasks.size(); i++ )
			{
				((Task)doTasks.get(i)).perform();
			}
		}
		else
		{
			for ( Integer i = 0; i < otherwiseTasks.size(); i++ )
			{
				((Task)otherwiseTasks.get(i)).perform();
			}
		}
	}

	/**
	 * TaskContainer addTask() implementation. This is called by Ant for each child
	 * element nested under this one.
	 * @param child The child element added to this container
	 */
	public void addTask(Task child)
	{
		switch ( child.getTaskName() )
		{
			case "test:do":
				doTasks.add(child);
				break;
			case "test:otherwise":
				otherwiseTasks.add(child);
				break;
			default:
				System.out.println("WARNING: Unrecognized task added to <test:when>!");
		}
	}

	// Attribute Setters

	public void setValue(String value)
	{
		this.value = value;
	}

	public void setIs(String is)
	{
		this.expected = is;
	}

	public void setContains(String contains)
	{
		this.contains = contains;
	}

	// Helper Methods

	/**
	 * Determines which comparison method to use from the values of the attributes and then
	 * performs the comparison.
	 * @return The result of the comparison
	 */
	private Boolean doComparison()
	{
		if(expected != null)
		{
			return value.equals(expected);
		}

		if(contains != null)
		{
			return value.contains(contains);
		}

		return false;
	}
}
