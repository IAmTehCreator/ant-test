/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import java.util.*;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.TaskContainer;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.UnknownElement;
import org.apache.tools.ant.taskdefs.MacroDef;
import org.apache.tools.ant.taskdefs.Property;
import org.apache.tools.ant.RuntimeConfigurable;

/**
 * This class implements an Ant task that will mock a macro with a stubbed version
 * that can record invocations so that a unit test can assert that one unit of code
 * is correctly calling and passing arguments as expected.
 *
 * Supported Attributes;
 *  - macro  The name of the macro to be mocked
 *  - uri    The URI for the XML namespace that the macro is defined in
 *
 * Supported Elements;
 *  - attribute  An attribute element typically nested in a <macrodef> element.
 *  - test:when
 */
public class Mock extends Task implements TaskContainer
{
	private List attributes;
	private String macroName;
	private String macroUri;

	/**
	 * Default constructor
	 */
	public Mock()
	{
		super();

		attributes = new ArrayList();
	}

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		// Initialise mock recorder properties
		getProject().setProperty(getRecorderCallCountName(), Integer.toString(0));
		getProject().setProperty(getRecorderAtrributesName(), getAttributeNamesAsString());

		MacroDef mock = defineMacro(macroName, macroUri);
		addAttributesToMacro(mock);
		addCallRecorderImplementation(mock.createSequential());

		mock.perform();
	}

	/**
	 * TaskContainer addTask() implementation. This is called by Ant for each child
	 * element nested under this one.
	 * @param child The child element added to this container
	 */
	public void addTask(Task child)
	{
		attributes.add(child);
	}

	// Attribute Setters

	public void setMacro(String macro)
	{
		this.macroName = macro;
	}

	public void setUri(String uri)
	{
		this.macroUri = uri;
	}

	// Helper Methods

	/**
	 * Creates a <macrodef> task in the current project with the given name and
	 * URI.
	 * @param name The name of the macro that should be defined
	 * @param uri The URI of the XML namespace that the macro should belong to
	 * @return The <macrodef> class instance
	 */
	private MacroDef defineMacro(String name, String uri)
	{
		MacroDef macro = (MacroDef)getProject().createTask("macrodef");
		macro.setProject(getProject());
		macro.setName(name);

		if(uri != null)
		{
			macro.setURI(uri);
		}

		return macro;
	}

	/**
	 * Converts the nested <test:attribute> tasks into MacroDef.Attribute implementations
	 * and adds them to the given <macrodef> task.
	 * @param task The <macrodef> task to add the attributes to
	 */
	private void addAttributesToMacro(MacroDef task)
	{
		for(Integer i = 0; i < attributes.size(); i++)
		{
			UnknownElement element = (UnknownElement)attributes.get(i);

			task.addConfiguredAttribute(createAttribute(element));
		}
	}

	/**
	 * Creates tasks which implement a call recorder for the nested attributes of this
	 * task. The tasks will be added to the given <sequential> task.
	 * @param container The <sequential> task to add the call recorder implementation to
	 */
	private void addCallRecorderImplementation(MacroDef.NestedSequential container)
	{
		container.addTask(
			createIncrementTask( getRecorderCallCountName() )
		);

		for(String name : getAttributeNames())
		{
			container.addTask(
				createRecorderTask(name)
			);
		}
	}

	/**
	 * Creates a MacroDef.Attribute implementation from a <test:attribute> element.
	 * @param element The UnknownElement element to retrieve the name and default value from
	 * @return The configured MacroDef.Attribute class
	 */
	private MacroDef.Attribute createAttribute(UnknownElement element)
	{
		RuntimeConfigurable configuredElement = element.getWrapper();
		Hashtable configuredAttributes = configuredElement.getAttributeMap();

		if(!configuredAttributes.containsKey(Attribute.ATTRIBUTE_NAME))
		{
			throw new BuildException("Must specify the name of the attribute");
		}

		MacroDef.Attribute converted = new MacroDef.Attribute();
		converted.setName((String)configuredAttributes.get(Attribute.ATTRIBUTE_NAME));

		if(configuredAttributes.containsKey(Attribute.ATTRIBUTE_DEFAULT))
		{
			converted.setDefault((String)configuredAttributes.get(Attribute.ATTRIBUTE_DEFAULT));
		}

		return converted;
	}

	/**
	 * Iterates over the attribute elements nested under this element and returns
	 * the names for all of the attributes.
	 * @return A list of all supported attribute names
	 */
	private List<String> getAttributeNames()
	{
		List<String> names = new ArrayList<String>();
		for(Integer i = 0; i < attributes.size(); i++)
		{
			UnknownElement element = (UnknownElement)attributes.get(i);
			RuntimeConfigurable configuredElement = element.getWrapper();
			Hashtable configuredAttributes = configuredElement.getAttributeMap();

			names.add(
				(String)configuredAttributes.get(Attribute.ATTRIBUTE_NAME)
			);
		}

		return names;
	}

	/**
	 * Gets a serialised version of all attribute names where each name is separated
	 * by a comma.
	 * @return A comma seperated list of attribute names
	 */
	private String getAttributeNamesAsString()
	{
		String nameString = "";
		for(String name : getAttributeNames())
		{
			if(nameString == null)
			{
				nameString = name;
			}
			else
			{
				nameString = nameString + "," + name;
			}
		}

		return nameString;
	}

	/**
	 * Creates a new <test:increment> task for the given variable.
	 * @param varName The name of the property to increment
	 * @return The <test:increment> element ready to be added to a container
	 */
	private UnknownElement createIncrementTask(String varName)
	{
		UnknownElement element = new UnknownElement("increment");
		element.setNamespace("com.smilne.test");
		element.setProject(getProject());
		element.setQName("test:increment");

		RuntimeConfigurable configuration = new RuntimeConfigurable(element, "increment");
		configuration.setAttribute("var", varName);

		return element;
	}

	/**
	 * Creates a <test:record /> task for the given attribute.
	 * @param attribute The name of the attribute to record
	 * @return The <test:record /> element ready to be added to a container
	 */
	private UnknownElement createRecorderTask(String attribute)
	{
		UnknownElement element = new UnknownElement("record");
		element.setNamespace("com.smilne.test");
		element.setProject(getProject());
		element.setQName("test:record");

		RuntimeConfigurable configuration = new RuntimeConfigurable(element, "record");
		configuration.setAttribute("attribute", attribute);
		configuration.setAttribute("mock", macroName);
		configuration.setAttribute("value", "@{" + attribute + "}");

		return element;
	}

	/**
	 * Returns the generated property name for the call count for this mock.
	 * @return The generated property name
	 */
	private String getRecorderCallCountName()
	{
		return "mocks." + macroName + ".calls";
	}

	/**
	 * Returns the generated property name for the given attribute on the current
	 * call index.
	 * @param attribute The name of the attribute to generate the property name for
	 * @param callIndex The call number starting from 1
	 * @return The generated property name
	 */
	private String getRecorderPropertyName(String attribute, String callIndex)
	{
		return "mocks." + macroName + ".call." + callIndex +  "." + attribute;
	}

	/**
	 * Returns the generated property name for the attributes list.
	 * @return The generated property name
	 */
	private String getRecorderAtrributesName()
	{
		return "mocks." + macroName + ".attributes";
	}
}
