/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import org.apache.tools.ant.Task;

/**
 * This class implements an Ant task which will increment the specified counter by the specified
 * amount. If the amount is not specified then the counter is incremented by 1.
 *
 * Supported Attributes;
 *  - var  The name of the property that contains the counter
 *  - by   Optional, the amount to increment the counter by
 */
public class Increment extends Task
{
	public String var;
	public Integer by;

	/**
	 * Default constructor
	 */
	public Increment()
	{
		super();

		by = 1;
	}

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		String rawValue = getProject().getProperty(var);

		Integer value = rawValue == null ? 0 : Integer.valueOf(rawValue);
		Integer result = value + by;

		getProject().setProperty(var, Integer.toString(result));
	}

	// Attribute Setters

	public void setVar(String var)
	{
		this.var = var;
	}

	public void setBy(String by)
	{
		this.by = Integer.valueOf(by);
	}
}
