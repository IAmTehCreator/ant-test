/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import java.util.*;
import org.apache.tools.ant.Task;

/**
 * This class implements an Ant task that will take a mock name and reset all
 * properties it's call recorder has created.
 *
 * Supported Attributes;
 *  - mock  The name of the mock whose recorder is to be reset
 */
public class MockReset extends Task
{
	private String mockName;

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		String callsVariable = getRecorderCallCountName(mockName);
		Integer calls = Integer.valueOf( getProject().getProperty(callsVariable) );

		// Clear the number of times the mock has been invoked
		getProject().setProperty(callsVariable, Integer.toString(0));

		// Clear the attribute recordings for each invocation
		for(String attribute : getAttributeList())
		{
			for(Integer i = 1; i <= calls; i++)
			{
				getProject().setProperty( getRecorderPropertyName(mockName, attribute, i), "");
			}
		}
	}

	// Attribute Setters

	public void setMock(String mockName)
	{
		this.mockName = mockName;
	}

	// Helper Methods

	/**
	 * Gets the serialised mock macro attribute names and returns them as a list
	 * of names.
	 * @return A list of attribute names that the mock uses
	 */
	private String[] getAttributeList()
	{
		String attributes = getProject().getProperty( getRecorderAtrributesName(mockName) );

		return attributes.split(",");
	}

	/**
	 * Returns the generated property name for the call count for this mock.
	 * @param name The name of the mock
	 * @return The generated property name
	 */
	private String getRecorderCallCountName(String name)
	{
		return "mocks." + name + ".calls";
	}

	/**
	 * Returns the generated property name for the given attribute on the current
	 * call index.
	 * @param name The name of the mock
	 * @param attribute The name of the attribute to generate the property name for
	 * @param callIndex The call number starting from 1
	 * @return The generated property name
	 */
	private String getRecorderPropertyName(String name, String attribute, Integer callIndex)
	{
		return "mocks." + name + ".call." + callIndex +  "." + attribute;
	}

	/**
	 * Returns the generated property name for the attributes list.
	 * @param name The name of the mock
	 * @return The generated property name
	 */
	private String getRecorderAtrributesName(String name)
	{
		return "mocks." + name + ".attributes";
	}
}
