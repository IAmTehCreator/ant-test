/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import java.util.*;
import org.apache.tools.ant.Task;

import com.smilne.Logger;

/**
 * This class implements an Ant task which logs ANSI styled messages to the console. By default
 * all messages will be printed in bright white but this can be changed using the attributes. This
 * task observes the indentation set by com.smilne.test.Logger and calls to Logger.indent() and
 * Logger.dedent() will affect the message logged.
 *
 * ANSI colours and attributes can be specified by text or by integer, for example for magenta text
 * you could specify <log colour="magenta" /> or <log colour="35" />. Below is a list of the colour
 * and attribute values supported;
 *
 * Colour: black/red/green/yellow/magenta/cyan/white
 * Attribute: reset/bright/dim/underline/link/reverse/hidden
 *
 * Supported Attributes;
 *  - message  The message to print to the console
 *  - colour  Optional, the foreground colour of the message
 *  - attribute  Optional, a modifier for the message font
 */
public class Log extends Task
{
	private static final Map<String, Integer> COLOURS = new HashMap<String, Integer>()
	{{
		put("black", Logger.COLOUR_BLACK);
		put("red", Logger.COLOUR_RED);
		put("green", Logger.COLOUR_GREEN);
		put("yellow", Logger.COLOUR_YELLOW);
		put("magenta", Logger.COLOUR_MAGENTA);
		put("cyan", Logger.COLOUR_CYAN);
		put("white", Logger.COLOUR_WHITE);
	}};

	private static final Map<String, Integer> ATTRIBUTES = new HashMap<String, Integer>()
	{{
		put("reset", Logger.ATTRIBUTE_RESET);
		put("bright", Logger.ATTRIBUTE_BRIGHT);
		put("dim", Logger.ATTRIBUTE_DIM);
		put("underline", Logger.ATTRIBUTE_UNDERLINE);
		put("link", Logger.ATTRIBUTE_LINK);
		put("reverse", Logger.ATTRIBUTE_REVERSE);
		put("hidden", Logger.ATTRIBUTE_HIDDEN);
	}};

	public String message;
	public Integer colour;
	public Integer attribute;

	/**
	 * Default constructor
	 */
	public Log()
	{
		super();

		colour = Logger.COLOUR_WHITE;
		attribute = Logger.ATTRIBUTE_BRIGHT;
	}

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		Logger.log(this.message, this.colour, this.attribute);
	}

	// Attribute Setters

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setColour(String colour)
	{
		this.colour = COLOURS.get(colour);

		if(this.colour == null)
		{
			this.colour = Integer.valueOf(colour);
		}
	}

	public void setAttribute(String attribute)
	{
		this.attribute = ATTRIBUTES.get(attribute);

		if(this.attribute == null)
		{
			this.attribute = Integer.valueOf(attribute);
		}
	}
}
