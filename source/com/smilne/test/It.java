/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import java.util.*;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.TaskContainer;

import com.smilne.Logger;

/**
 * This class implements an Ant task which contains the implementation of a unit test for part
 * of the build system. This should contain a small test which asserts a specific item of
 * functionallity using the <test:assert /> task. This task can contain any other Ant tasks.
 *
 * Individual tests can be disabled or whitelisted by specifying "true" for the appropriate
 * attributes.
 *
 * Supported attributes are;
 *  - should     The message displayed when the test is run (prefixed with "Should ")
 *  - disabled   Optional, when "true" the test will not be run
 *  - whitelist  Optional, when "true" if the test fails then the build will not be failed
 */
public class It extends Task implements TaskContainer
{
	private List steps;

	private Boolean isDisabled;
	private Boolean isWhitelisted;
	private String message;

	/**
	 * Default constructor
	 */
	public It()
	{
		super();

		steps = new ArrayList();
		isDisabled = false;
		isWhitelisted = false;
		message = "";
	}

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		if(isDisabled)
		{
			Logger.log("Should " + message + " (disabled)", Logger.COLOUR_WHITE, Logger.ATTRIBUTE_DIM);
			return;
		}

		if(isWhitelisted)
		{
			Logger.log("Should " + message + " (whitelisted)", Logger.COLOUR_WHITE, Logger.ATTRIBUTE_DIM);
		}
		else
		{
			Logger.log("Should " + message);
		}

		try
		{
			Logger.indent();

			executeChildren();
		}
		catch(Exception e)
		{
			if(!isWhitelisted)
			{
				throw e;
			}
			else if(!e.getMessage().startsWith("FAILED: "))
			{
				Logger.log("EXCEPTION: " + e.getMessage());
			}
		}
		finally
		{
			Logger.dedent();
		}
	}

	/**
	 * TaskContainer addTask() implementation. This is called by Ant for each child
	 * element nested under this one.
	 * @param child The child element added to this container
	 */
	public void addTask(Task child)
	{
		steps.add(child);
	}

	// Attribute Setters

	public void setDisabled(String value)
	{
		this.isDisabled = value.equals("true");
	}

	public void setWhitelist(String value)
	{
		this.isWhitelisted = value.equals("true");
	}

	public void setShould(String value)
	{
		this.message = value;
	}

	// Helper Methods

	/**
	 * Iterates over all child elements and invokes them.
	 */
	private void executeChildren()
	{
		for ( Integer i = 0; i < steps.size(); i++ )
		{
			((Task)steps.get(i)).perform();
		}
	}
}
