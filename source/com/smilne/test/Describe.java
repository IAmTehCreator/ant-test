/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import java.util.*;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.TaskContainer;
import org.apache.tools.ant.UnknownElement;
import org.apache.tools.ant.RuntimeConfigurable;

import com.smilne.Logger;

/**
 * This class implements an Ant task which acts as a container for unit tests which can be nested
 * allowing tests for similar areas of the build system to be grouped together and controlled as
 * one. The describe task can be disabled which will prevent all tests contained within from being
 * run. It can also be whitelisted so contained tests are run but the build is not failed if any
 * of the tests fail.
 *
 * The <test:describe> task supports the following child elements;
 *  - <test:describe>     Nested groups of tests
 *  - <test:before-all>   Contains tasks which will be run before any of the tests have started
 *  - <test:before-each>  Contains tasks which will be run before each test
 *  - <test:it>           Contains the implementation of a single test
 *  - <test:after-each>   Contains tasks which will be run after each test
 *  - <test:after-all>    Contains tasks which will be run after all of the tests have finished
 *
 * Supported attributes are;
 *  - name       The name of the group
 *  - disabled   Optional attribute, when "true" none of the contained tests are run
 *  - whitelist  Optional, when "true" failing tests will not fail the build
 */
public class Describe extends Task implements TaskContainer
{
	private static final String TASK_BEFORE_ALL = "test:before-all";
	private static final String TASK_BEFORE_EACH = "test:before-each";
	private static final String TASK_AFTER_ALL = "test:after-all";
	private static final String TASK_AFTER_EACH = "test:after-each";

	protected List tests;

	private Task beforeAll;
	private Task beforeEach;
	private Task afterAll;
	private Task afterEach;

	private String name;
	private Boolean isDisabled;
	private Boolean isWhitelisted;

	/**
	 * Default constructor
	 */
	public Describe()
	{
		super();

		tests = new ArrayList();
		isDisabled = false;
		isWhitelisted = false;
	}

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		if(isDisabled)
		{
			Logger.log("Describe " + name + " (disabled)", Logger.COLOUR_CYAN, Logger.ATTRIBUTE_DIM);
		}
		else
		{
			if(isWhitelisted)
			{
				Logger.log("Describe " + name + " (whitelisted)", Logger.COLOUR_CYAN, Logger.ATTRIBUTE_DIM);
			}
			else
			{
				Logger.log("Describe " + name, Logger.COLOUR_CYAN);
			}

			Logger.indent();
			executeChildren();
			Logger.dedent();
		}

		Logger.log("");
	}

	/**
	 * TaskContainer addTask() implementation. This is called by Ant for each child
	 * element nested under this one.
	 * @param child The child element added to this container
	 */
	public void addTask(Task child)
	{
		if(child.getTaskName() == TASK_BEFORE_ALL)
		{
			beforeAll = child;
			return;
		}

		if(child.getTaskName() == TASK_BEFORE_EACH)
		{
			beforeEach = child;
			return;
		}

		if(child.getTaskName() == TASK_AFTER_ALL)
		{
			afterAll = child;
			return;
		}

		if(child.getTaskName() == TASK_AFTER_EACH)
		{
			afterEach = child;
			return;
		}

		tests.add(child);
	}

	// Attribute Setters

	public void setName(String name)
	{
		this.name = name;
	}

	public void setDisabled(String value)
	{
		this.isDisabled = value.equals("true");
	}

	public void setWhitelist(String value)
	{
		this.isWhitelisted = value.equals("true");
	}

	// Helper Methods

	/**
	 * Iterates over all child elements and invokes them while invoking the test
	 * lifecycle tasks (before-each etc.).
	 */
	private void executeChildren()
	{
		Project describeProject = getProject().createSubProject();

		if(beforeAll != null)
		{
			changeProject(describeProject, (UnknownElement)beforeAll);
			beforeAll.perform();
		}

		for ( Integer i = 0; i < tests.size(); i++ )
		{
			if(beforeEach != null)
			{
				changeProject(describeProject, (UnknownElement)beforeEach);
				beforeEach.perform();
			}

			try
			{
				Task it = (Task)tests.get(i);
				changeProject(describeProject, (UnknownElement)it);
				it.perform();
			}
			catch(Exception e)
			{
				if(!isWhitelisted)
				{
					throw e;
				}
				else if(!e.getMessage().startsWith("FAILED: "))
				{
					Logger.log("EXCEPTION: " + e.getMessage(), Logger.COLOUR_RED);
				}
			}

			if(afterEach != null)
			{
				changeProject(describeProject, (UnknownElement)afterEach);
				afterEach.perform();
			}
		}

		if(afterAll != null)
		{
			changeProject(describeProject, (UnknownElement)afterAll);
			afterAll.perform();
		}
	}

	/**
	 * Assigns the given element and all child elements to the new project provided.
	 * @param newProject The project to be assigned to the element and child elements
	 * @param container The element whose project should be recursively changed
	 */
	private void changeProject(Project newProject, UnknownElement container)
	{
		container.setProject(newProject);
		RuntimeConfigurable config = container.getWrapper();

		Enumeration enumeration = config.getChildren();
		while ( enumeration.hasMoreElements() )
		{
			RuntimeConfigurable childConfig = (RuntimeConfigurable) enumeration.nextElement();
			UnknownElement childElement = (UnknownElement)childConfig.getProxy();

			if(childElement == null)
				return;

			childElement.setProject(newProject);
			changeProject(newProject, childElement);
		}
	}
}
