/**
 * Copyright (c) 2018 by Steven Ian Milne, see LICENSE for details
 */
package com.smilne.test;

import org.apache.tools.ant.Task;

/**
 * This class implements an Ant task which will record an attribute value from
 * a mock invocation.
 *
 * NOTE: Internal class used by <test:mock> do not use directly in build scripts
 */
public class Record extends Task
{
	public String mock;
	public String attribute;
	public String value;

	/**
	 * Ant task entry point. Invoked by Ant to perform the logic for this task.
	 */
	public void execute()
	{
		getProject().setProperty(getRecorderPropertyName(), value);
	}

	// Attribute Setters

	public void setMock(String mock)
	{
		this.mock = mock;
	}

	public void setAttribute(String attribute)
	{
		this.attribute = attribute;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	// Helper Methods

	/**
	 * Gets the current number of times the mock has been invoked.
	 * @return The number of times the mock has been invoked
	 */
	private Integer getCallIndex()
	{
		return Integer.valueOf(
			getProject().getProperty("mocks." + mock + ".calls")
		);
	}

	/**
	 * Gets the name of the property that the recorder value should be stored in.
	 * @return The property name
	 */
	private String getRecorderPropertyName()
	{
		return "mocks." + mock + ".call." + getCallIndex() +  "." + attribute;
	}
}
